A Symfony2 RabbitMQ wrapper.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/stripebundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\StripeBundle\XLabsStripeBundle(),
    ];
}
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_stripe:
    public_key: <PARAM>
    secret: <PARAM>
```