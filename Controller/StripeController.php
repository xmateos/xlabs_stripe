<?php

namespace XLabs\StripeBundle\Controller;

use Stripe\Checkout\Session as StripeSession;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Route(host="www.wap.nl")
 * @Route("/stripe")
 */
class StripeController extends AbstractController
{
    /**
     * @Route("/checkout", name="stripe_checkout", options={"expose"=true})
     */
    public function createCheckoutSession(Request $request): Response
    {
        Stripe::setApiKey($_ENV['STRIPE_SECRET_KEY']);

        $session = StripeSession::create([
            'payment_method_types' => ['card', 'paypal'],
            'line_items' => [[
                'price_data' => [
                    'currency' => 'usd',
                    'product_data' => [
                        'name' => 'T-shirt',
                        'description' => 'Some words here.',
                        'images' => ['https://images.freeimages.com/images/large-previews/ac9/gallery-3-1220725.jpg?fmt=webp&w=500']
                    ],
                    'unit_amount' => 2000,
                ],
                'quantity' => 1,
            ],[
                'price_data' => [
                    'currency' => 'usd',
                    'product_data' => [
                        'name' => 'Shoes',
                        'description' => 'Some words here.',
                        'images' => ['https://images.freeimages.com/images/large-previews/b97/fine-italian-food-gallery-5-1519693.jpg?fmt=webp&w=500']
                    ],
                    'unit_amount' => 1500,
                ],
                'quantity' => 2,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('stripe_payment_success', [], \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('stripe_payment_cancel', [], \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_URL),
            'customer_email' => 'x@x.com',
            'metadata' => [
                'user_id' => '123456',
                'order_reference' => 'ABCDEF',
            ],
        ]);

        return $this->redirect($session->url, 303);
    }

    /**
     * @Route("/payment-success", name="stripe_payment_success", options={"expose"=true})
     */
    public function paymentSuccess(Request $request): Response
    {
        // Handle successful payment here
        dd($request);
        return new Response('Payment successful.');
    }

    /**
     * @Route("/payment-cancel", name="stripe_payment_cancel", options={"expose"=true})
     */
    public function paymentCancel(Request $request): Response
    {
        // Handle payment cancellation here
        dd($request);
        return new Response('Payment cancelled.');
    }
}